import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

// will allow us to use bootstrap classes
import 'bootstrap/dist/css/bootstrap.min.css';


ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);


/*

const name = "Emma Bumaa";

const user = {
  firstName: "Chris",
  lastName: "Pratt"
}

const formatName = (user) => {
  return `${user.firstName} ${user.lastName}`
}

// JSX allows us to use an html tag in a js file
const element = <h1>Hello my name is, {formatName(user)} </h1>

ReactDOM.render(
    element,
    document.getElementById("root")
);
*/
// changes upon saving :)
// error shows in browser right away


