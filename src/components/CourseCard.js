import {useState, useEffect} from 'react';
import { Card, Button } from 'react-bootstrap';
import {Link} from 'react-router-dom';

// prop- property
export default function CourseCard ({courseProp}) {

	console.log(courseProp);
	console.log(typeof courseProp);
	// courseProp is an object- you can destructure
/*
	const [count, setCount] = useState(0);
	//syntax: const [getter, setter] = useState(initialValueGetter)

	function enroll(){
		setCount(count + 1)
		console.log('Enrollees' + count)
	};
*/
/*
// mini activity
	const [count, setCount] = useState(0);
	const [seats, setSeat] = useState(30);
	

	function enroll(){
		if (seats > 0){
		setCount(count + 1)	
		console.log('Enrollees' + count)
		setSeat(seats - 1)
		console.log('Seats' + seats)	
	} /*else {
		alert("no more seats")
		};	
	};
*/
/*	useEffect(() => {
		if(seats === 0) {
			alert("No more seats available")
		}
	}, [seats])*/
	//syntax: useEffect(() => {}, [optionalParameter])


	//destructure courseprop to get value
	const {name, description, price, _id} = courseProp

	return(
		<Card className="mt-3 mb-3">
			<Card className="cardCourse p-3">
					<Card.Body> 
						<Card.Title>{name}</Card.Title>					
						<Card.Subtitle>Description</Card.Subtitle>
						<Card.Text> {description}</Card.Text>
						<Card.Subtitle>Price</Card.Subtitle>
						<Card.Text>{price}</Card.Text>
						<Button variant="primary" as={Link} to={`/courses/${_id}`}>See Details</Button>
					</Card.Body>
				</Card>
		</Card>
	)
};

