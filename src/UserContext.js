/*
Context API in React 
	-importation from a  PROVIDER DIRECTLY TO CONSUMER
	- to avoid prop drilling

prop drilling :(

*/

import React from 'react';

// create provider/ context object

	// context object is a data type of an object that can be used to store information that can be shared to other components within app
	// context object is a different approach to passing info between components and allows us for easier access by avoiding the use of prop drilling

const UserContext = React.createContext();

// The "Provider" component allows other components to consume the context object and supply the necessary info needed by the context object

export const UserProvider = UserContext.Provider;

export default UserContext;