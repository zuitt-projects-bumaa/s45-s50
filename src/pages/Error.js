import{ Row, Col, Button } from 'react-bootstrap';
/*
export default function Error(){
	return(
		<Row>
			<Col className = "p-5">
				<h1>Page Not Found</h1>
				<p>Go back to <a href= "/">homepage</a></p>

			</Col>
		</Row>
	)
}

*/
import Banner from '../components/Banner'

export default function Error() {

	const data = {
		title: "404: Page Not Found",
		content: "The page you are looking for does not exist",
		destination: "/",
		label: "Back to Homepage"
	}

	return(
		<Banner data={data}/>
	)
}
